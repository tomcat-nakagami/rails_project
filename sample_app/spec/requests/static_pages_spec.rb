require 'spec_helper'

RSpec.describe "静的ページ", type: :request do
  # subject { page }
  #
  # shared_examples_for "all static pages" do
  #   it { should have_content(heading) }
  #   it { should have_title(full_title(page_title)) }
  # end

  describe "Homeページ 正常テスト" do
    # let(:heading)    { 'Sample App' }
    # let(:page_title) { '' }

    # have_likeがメソッドエラーでたのでとりあえずコメントアウト
    # it_should_not have_like "all static pages"

    # it { should_not have_title('| Home') }

    describe "for signed-in users" do
      let(:user) { FactoryGirl.create(:user) }
      before do
        FactoryGirl.create(:micropost, user: user, content: "Lorem")
        FactoryGirl.create(:micropost, user: user, content: "Ipsum")
        sign_in user
        visit root_path
      end

      it "should render the user's feed" do
        user.feed.each do |item|
          expect(page).to have_selector("li##{item.id}", text: item.content)
        end
      end

      describe "follower/following counts" do
        let(:other_user) { FactoryGirl.create(:user) }
        before do
          other_user.follow!(user)
          visit root_path
        end

        it { should have_link("0 following", href: following_user_path(user)) }
        it { should have_link("1 followers", href: followers_user_path(user)) }
      end
    end
  end

  it "タイトルが表示される" do
    visit root_path
    expect(page).to have_content("Home")
  end

  describe "Help page" do
    before { visit help_path }

    it { should have_content('Help') }
    it { should have_title(full_title('Help')) }
  end

  describe "About page" do
    before { visit about_path }

    it { should have_content('About') }
    it { should have_title(full_title('About Us')) }
  end

  describe "Contact page" do
    before { visit contact_path }

    it { should have_content('Contact') }
  end
end
