module SessionsHelper

  def sign_in(user)
    remember_token = User.new_remember_token

    # ブラウザくんのクッキーにトークン渡す
    cookies.permanent[:remember_token] = remember_token

    # アプリケーション側にも、上で渡したトークンを保存しておく(暗号化)
    user.update_attribute(:remember_token, User.encrypt(remember_token))

    self.current_user = user
  end

  def signed_in?
    !current_user.nil?
  end

  def current_user=(user)
    @current_user = user
  end

  def current_user
    remember_token = User.encrypt(cookies[:remember_token])
    # TODO あとでdashに不思議な記号シリーズで登録しておく
    @current_user ||= User.find_by(remember_token: remember_token)
  end

  def current_user?(user)
    user == current_user
  end

  def signed_in_user
    unless signed_in?
      store_locationredirect_to signin_url, notice: "Please sign in"
    end
  end

  def sign_out
    self.current_user = nil
    cookies.delete(:remember_token)
  end

  def redirect_back_or(default)
    redirect_to(session[:return_to] || default)
    session.delete(:return_to)
  end

  def store_location
    session[:return_to] = request.url
  end
end
